import { Questionnaire } from "@/types";

export const QUESTIONNAIRE: Questionnaire = {
  id: 0,
  title: "Discover Your Personality Type: Introvert or Extrovert?",
  description:
    "Are you the life of the party, or do you prefer to sit back and observe? Do you recharge by spending time alone, or do you need to be around others to feel energized? Find out if you're an introvert or an extrovert with this fun and playful multiple choice test!",
  questions: [
    {
      id: 1,
      content: "When you are in a group of people, do you:",
      answers: [
        {
          id: 1,
          content:
            "Tend to be the one who is talking the most and leading the conversation.",
          scores: {
            1: 0,
            2: 100,
          },
        },
        {
          id: 2,
          content:
            "Usually listen more than you speak, and prefer one-on-one conversations.",
          scores: {
            1: 100,
            2: 0,
          },
        },
      ],
    },
    {
      id: 2,
      content:
        "When you have to give a presentation in front of a group, do you:",
      answers: [
        {
          id: 5,
          content:
            "Usually feel anxious and uncomfortable, and prefer to have detailed notes or a script to follow.",
          scores: {
            1: 100,
            2: 0,
          },
        },
        {
          id: 6,
          content:
            "Tend to feel energized and excited, and enjoy improvising and interacting with the audience.",
          scores: {
            1: 0,
            2: 100,
          },
        },
      ],
    },
    {
      id: 3,
      content: "When you are in a new social situation, do you:",
      answers: [
        {
          id: 9,
          content:
            "Usually take some time to observe and get a sense of the dynamics before joining in.",
          scores: {
            1: 100,
            2: 0,
          },
        },
        {
          id: 10,
          content:
            "Tend to quickly introduce yourself and start making connections with the other people.",
          scores: {
            1: 0,
            2: 100,
          },
        },
      ],
    },
    {
      id: 4,
      content: "When you are making a decision, do you:",
      answers: [
        {
          id: 13,
          content:
            "Tend to go with your gut instincts and make a quick decision.",
          scores: {
            1: 0,
            2: 100,
          },
        },
        {
          id: 14,
          content:
            "Usually consider your options carefully and take your time before deciding.",
          scores: {
            1: 100,
            2: 0,
          },
        },
      ],
    },
    {
      id: 5,
      content: "When you are in a crowded or noisy place, do you:",
      answers: [
        {
          id: 13,
          content:
            "Usually feel overwhelmed and need to take some time alone to recharge.",
          scores: {
            1: 100,
            2: 0,
          },
        },
        {
          id: 14,
          content:
            "Tend to thrive in the energy of the crowd and enjoy the excitement.",
          scores: {
            1: 0,
            2: 100,
          },
        },
      ],
    },
  ],
  results: {
    1: {
      id: 1,
      title: "You're more of an introvert",
      description:
        "According to the results of the questionnaire, you are an introvert, which means that you tend to be more reserved, quiet, and inward-focused in your demeanor and behavior. You may find social situations and interactions with others to be draining and may prefer to spend time alone or in smaller, more intimate groups. You may also be more reflective and thoughtful in your decision-making and may enjoy activities that allow you to engage with your own thoughts and ideas, such as reading, writing, or artistic pursuits. Being an introvert does not necessarily mean that you are shy or lack confidence, but rather that you have a unique way of engaging with the world and may need to recharge after spending time with others.",
    },
    2: {
      id: 2,
      title: "You're more of an extrovert",
      description:
        "According to the results of the questionnaire, you are an extrovert, which means that you tend to be more outgoing, energetic, and sociable in your demeanor and behavior. You may find social situations and interactions with others to be energizing and may enjoy being the center of attention or engaging in group activities. You may also be more impulsive and spontaneous in your decision-making and may enjoy activities that allow you to be active and explore the world, such as traveling, sports, or outdoor adventures. Being an extrovert does not necessarily mean that you are loud or overly talkative, but rather that you have a unique way of engaging with the world and may need to recharge after spending time alone.",
    },
  },
};
