import { Questionnaire } from "@/types";
import { QUESTIONNAIRE } from "./questionnaire";

export const getQuestionnares = (): Questionnaire[] => {
  return [QUESTIONNAIRE];
};

export const getQuestionnare = (id: number) => {
  switch (id) {
    case 0:
      return QUESTIONNAIRE;
    default:
      return null;
  }
};

export const getResult = (resultId: string, questionnaireId: number) => {
  switch (questionnaireId) {
    case 0:
      return QUESTIONNAIRE.results[resultId];
    default:
      return null;
  }
};

export const getQuestionnairePaths = () => {
  const questionnaires = getQuestionnares();

  const paths: { params: Object }[] = [];

  questionnaires.forEach((questionnaire) => {
    paths.push({ params: { id: String(questionnaire.id) } });
  });

  return paths;
};

export const getResultPaths = () => {
  const questionnaires = getQuestionnares();

  const paths: { params: Object }[] = [];

  questionnaires.forEach((questionnaire) => {
    Object.values(questionnaire.results).map((result) => {
      paths.push({
        params: {
          id: String(questionnaire.id),
          result: "result",
          resultId: String(result.id),
        },
      });
    });
  });

  return paths;
};
