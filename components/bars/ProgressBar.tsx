type Props = {
  length: number;
  progressIndex: number;
};

export const ProgressBar = ({ length, progressIndex }: Props) => {
  const progress = (progressIndex / length) * 100;

  return <div className="progress" style={{ width: `${progress}%` }} />;
};
