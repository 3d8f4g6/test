type Props = {
  content: string;
  href: string;
};

export const Link = ({ content, href }: Props) => {
  return (
    <a className="link" href={href}>
      {content}
    </a>
  );
};
