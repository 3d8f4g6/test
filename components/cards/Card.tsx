type Props = {
  content: string;
  href?: string;
  onClick?: () => void;
};

export const Card = ({ content, href, onClick }: Props) => {
  return (
    <a
      className={`card fade-in ${onClick || href ? "clickable" : ""}`}
      href={href}
      onClick={onClick}
    >
      {content}
    </a>
  );
};
