import Head from "next/head";
import { ReactNode } from "react";
import { ProgressBar } from "@/components/bars/ProgressBar";
import { BackButton } from "@/components/buttons/BackButton";
import { useRouter } from "next/router";

type Props = {
  title: string;
  subtitle?: ReactNode;
  index?: number;
  length?: number;
  hideNavigation?: boolean;
  children?: ReactNode;
};

const BaseView = ({
  title,
  subtitle,
  hideNavigation,
  index,
  length,
  children,
}: Props) => {
  const router = useRouter();

  return (
    <>
      <Head>
        <title>Persona</title>
        <meta name="description" content={title} />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className="main">
        <ProgressBar length={length ?? 1} progressIndex={index ?? 0} />
        {!hideNavigation && <BackButton onClick={router.back} />}
        <div className="container">
          <div className="title">
            <h1>{title}</h1>
            <p>{subtitle}</p>
          </div>
          <div className="content fade-in">{children}</div>
        </div>
      </main>
    </>
  );
};

export default BaseView;
