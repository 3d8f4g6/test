import BaseView from "@/components/views/BaseView";

export default function ErrorPage() {
  return <BaseView title="404" subtitle="This page could not be found..." />;
}
