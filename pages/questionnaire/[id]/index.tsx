import { getQuestionnairePaths, getQuestionnare } from "@/database";
import { QuestionnaireView } from "@/features/questionnaire";
import { Questionnaire } from "@/types";
import { GetStaticProps } from "next";

interface Props {
  questionnaire: Questionnaire;
}

export default function QuestionnairePage(props: Props) {
  return <QuestionnaireView {...props} />;
}

export async function getStaticPaths() {
  const paths = getQuestionnairePaths();

  return {
    paths,
    fallback: false,
  };
}

export const getStaticProps: GetStaticProps<Props> = async (context) => {
  const id = Number(context.params?.id);

  const questionnaire = getQuestionnare(Number(id));

  if (!questionnaire) {
    return { notFound: true };
  }

  return { props: { questionnaire } };
};
