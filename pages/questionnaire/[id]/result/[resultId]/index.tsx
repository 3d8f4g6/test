import { getResult, getResultPaths } from "@/database";
import { ResultView } from "@/features/questionnaire";
import { Result } from "@/types";
import { GetStaticProps } from "next";

type Props = {
  questionnaireId: number;
  result: Result;
};

export default function QuestionnaireResultPage(props: Props) {
  return <ResultView {...props} />;
}

export async function getStaticPaths() {
  const paths = getResultPaths();

  return {
    paths,
    fallback: false,
  };
}

export const getStaticProps: GetStaticProps<Props> = async ({ params }) => {
  const questionnaireId = Number(params?.id);
  const resultId = params!.resultId as string;

  const result = getResult(resultId, questionnaireId);

  if (!result) {
    return { notFound: true };
  }

  return { props: { questionnaireId, result } };
};
