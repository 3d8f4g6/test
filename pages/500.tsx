import BaseView from "@/components/views/BaseView";

export default function ErrorPage() {
  return <BaseView title="500" subtitle="Server-side error occurred..." />;
}
