import { getQuestionnares } from "@/database";
import { HomeView } from "@/features/home";
import { Questionnaire } from "@/types";
import { GetStaticProps } from "next";

type Props = {
  questionnaires: Questionnaire[];
};

export default function HomePage(props: Props) {
  return <HomeView {...props} />;
}

export const getStaticProps: GetStaticProps<Props> = async () => {
  const questionnaires = getQuestionnares();

  return { props: { questionnaires } };
};
