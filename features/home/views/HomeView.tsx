import { Card } from "@/components";
import { Questionnaire } from "@/types";
import BaseView from "../../../components/views/BaseView";

type Props = {
  questionnaires: Questionnaire[];
};

export const HomeView = ({ questionnaires }: Props) => {
  return (
    <BaseView title="Select a questionnaire:" hideNavigation>
      {questionnaires.map((questionnaire) => {
        return (
          <Card
            key={questionnaire.id}
            content={questionnaire.title}
            href={`/questionnaire/${questionnaire.id}/start`}
          />
        );
      })}
      <div className={`card fade-in shaded`}>
        <p>{"What helps you find focus? (Coming soon)"}</p>
      </div>
      <div className={`card fade-in shaded`}>
        <p>{"How do you deal with change? (Coming soon)"}</p>
      </div>
    </BaseView>
  );
};
