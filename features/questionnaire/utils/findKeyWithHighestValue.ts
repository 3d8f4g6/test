export const findKeyWithHighestValue = (obj: Record<string, number>) => {
  let highestKey = null;
  let highestValue = -(-1 >>> 1);

  for (const key in obj) {
    if (obj.hasOwnProperty(key)) {
      if (obj[key] > highestValue) {
        highestKey = key;
        highestValue = obj[key];
      }
    }
  }

  return highestKey;
};
