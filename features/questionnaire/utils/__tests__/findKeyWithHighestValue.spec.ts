import { findKeyWithHighestValue } from "../findKeyWithHighestValue";

describe("findKeyWithHighestValue", () => {
  it("returns null for empty object", () => {
    const obj = {};
    expect(findKeyWithHighestValue(obj)).toBeNull();
  });

  it("returns correct key for non-empty object", () => {
    const obj = {
      key1: 10,
      key2: 20,
      key3: 15,
    };
    expect(findKeyWithHighestValue(obj)).toBe("key2");
  });

  it("returns first key for ties", () => {
    const obj = {
      key1: 10,
      key2: 20,
      key3: 20,
    };
    expect(findKeyWithHighestValue(obj)).toBe("key2");
  });

  it("returns correct key for negative numbers", () => {
    const obj = {
      key1: -10,
      key2: -20,
      key3: -15,
    };
    expect(findKeyWithHighestValue(obj)).toBe("key1");
  });
});
