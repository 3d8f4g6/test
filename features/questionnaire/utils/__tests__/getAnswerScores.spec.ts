import { getAnswerScores } from "../getAnswerScores";

describe("getAnswerScores", () => {
  describe("when called with a valid set of answers", () => {
    it("should return an object with the correct scores", () => {
      const answers = {
        1: {
          scores: {
            result1: 10,
            result2: 20,
          },
        },
        2: {
          scores: {
            result1: 5,
            result2: 15,
          },
        },
      };

      const expectedScores = {
        result1: 15,
        result2: 35,
      };

      expect(getAnswerScores(answers)).toEqual(expectedScores);
    });
  });

  describe("when called with an empty object", () => {
    it("should return an empty object", () => {
      const answers = {};
      expect(getAnswerScores(answers)).toEqual({});
    });
  });
});
