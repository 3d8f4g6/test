import { Answer } from "@/types";

export const getAnswerScores = (
  answers: Record<number, Pick<Answer, "scores">>
) => {
  const scores: Record<string, number> = {};

  Object.values(answers).forEach((answer) => {
    Object.entries(answer.scores).forEach(([resultId, score]) => {
      scores[resultId] = (scores[resultId] || 0) + score;
    });
  });

  return scores;
};
