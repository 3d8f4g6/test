import { Card } from "@/components";
import { Link } from "@/components/links/Link";
import BaseView from "@/components/views/BaseView";
import { Result } from "@/types";

interface Props {
  questionnaireId: number;
  result: Result;
}

export const ResultView = ({ questionnaireId, result }: Props) => {
  return (
    <BaseView title={result.title} length={1} index={1}>
      <Card content={result.description} />
      <Link content="try again →" href={`/`} />
    </BaseView>
  );
};
