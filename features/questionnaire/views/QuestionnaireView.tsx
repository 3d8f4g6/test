import { Card } from "@/components";
import { Link } from "@/components/links/Link";
import BaseView from "@/components/views/BaseView";
import { Questionnaire } from "@/types";
import { useTestPages } from "../hooks/useTestPages";

type Props = {
  isFirst?: boolean;
  questionnaire: Questionnaire;
};

export const QuestionnaireView = ({ isFirst, questionnaire }: Props) => {
  const { index, question, handleAnswer } = useTestPages(questionnaire);

  if (isFirst || !question) {
    return (
      <BaseView title={questionnaire.title}>
        <Card content={questionnaire.description} />
        <Link
          content="go to test →"
          href={`/questionnaire/${questionnaire.id}`}
        />
      </BaseView>
    );
  }

  return (
    <BaseView
      title={question.content}
      length={questionnaire.questions.length}
      index={index}
    >
      {question.answers.map((answer, index) => {
        return (
          <Card
            key={answer.id}
            content={answer.content}
            onClick={() => handleAnswer(index)}
          />
        );
      })}
    </BaseView>
  );
};
