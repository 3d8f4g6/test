import { Questionnaire } from "@/types";
import { useRouter } from "next/router";
import { findKeyWithHighestValue, getAnswerScores } from "../utils";

export const useTestPages = ({ id, questions }: Questionnaire) => {
  const router = useRouter();

  const answers: number[] = JSON.parse(
    (router.query.answers as string) ?? "[]"
  );

  const index = answers.length;
  const question = questions[index];

  const handleAnswer = (answer: number) => {
    const path = `/questionnaire/${id}`;
    const isLast = index + 1 >= questions.length;

    if (isLast) {
      const scores = getAnswerScores(
        answers.map((answerIndex, questionIndex) => {
          return questions[questionIndex].answers[answerIndex];
        })
      );

      const id = findKeyWithHighestValue(scores);

      return router.push(`${path}/result/${id}`);
    }

    return router.push(
      `${path}?answers=${JSON.stringify([...answers, answer])}`
    );
  };

  return { index, question, handleAnswer };
};
