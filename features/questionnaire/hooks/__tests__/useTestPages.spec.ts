import * as nextRouter from "next/router";
import { QUESTIONNAIRE } from "../../../../database/questionnaire";
import { useTestPages } from "../useTestPages";

const mockRouter = {
  route: "/",
  pathname: "",
  query: { answers: "[1,1,1]" } as any,
  asPath: "",
  push: jest.fn(),
  back: jest.fn(),
};

describe("useTestPages", () => {
  let hook: any;
  let spy: jest.SpyInstance;

  beforeEach(() => {
    spy = jest.spyOn(nextRouter, "useRouter");
    mockRouter.push = jest.fn();
    mockRouter.back = jest.fn();
    spy.mockImplementationOnce(() => mockRouter as any);
    hook = useTestPages(QUESTIONNAIRE);
  });

  it("returns index that equals the number of answers given", () => {
    expect(hook.index).toBe(3);
  });

  it("returns index of zero if answers not present in query params", () => {
    spy.mockImplementationOnce(() => ({ ...mockRouter, query: {} } as any));
    hook = useTestPages(QUESTIONNAIRE);
    expect(hook.index).toBe(0);
  });

  it("returns question in questionnaire corresponding to index", () => {
    expect(hook.question.id).toBe(QUESTIONNAIRE.questions[3].id);
  });

  describe("handleAnswer", () => {
    it("calls router with url containing answer in query param", () => {
      hook.handleAnswer(0);

      expect(mockRouter.push).toBeCalledTimes(1);
      expect(mockRouter.push).toBeCalledWith(
        "/questionnaire/0?answers=[1,1,1,0]"
      );
    });

    it("calls router with url for result when answer array reaches number of questions", () => {
      const answers = "[1,1,1,1]";
      spy.mockImplementationOnce(
        () => ({ ...mockRouter, query: { answers } } as any)
      );
      hook = useTestPages(QUESTIONNAIRE);
      hook.handleAnswer(0);

      expect(
        JSON.parse(answers).length + 1 === QUESTIONNAIRE.questions.length
      ).toBeTruthy();
      expect(mockRouter.push).toBeCalledTimes(1);
      expect(mockRouter.push).toBeCalledWith("/questionnaire/0/result/1");
    });
  });
});
