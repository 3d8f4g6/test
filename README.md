This is an example project for how to build a website that hosts different personality tests.

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Description

For overall structure, I'm loosely following the style of [Bulletproof React](https://github.com/alan2207/bulletproof-react). This gives a nice separation of concerns, rendering the project more scalable should I wish to add more features down the line.

The most complex feature is in the "questionnaire" directory. This contains view & business logic for the multiple choice questionnaires. I have written unit tests for the utility functions and hooks. These can be expanded upon in the future. I would also consider snapshot tests and E2E tests to prevent regression errors.

The state management of this logic is quite rudimentary. In a more complex application, I would consider lifting some of the question/answer logic to global state. However, the current model, which relies more on URL params is convenient for maintaining a navigation flow that is easily shareable.

This type of web app would work well as a static website so I'm making use of NextJS's built on static site generation features. I have not spent as much time on further optimisations (fix loads associated with fonts) or lighthouse scores but that's up next (along with more points covered [here](https://github.com/thedaviddias/Front-End-Checklist)). There are numerous ways to host this site. The quickest most convenient option is likely [Vercel](https://nextjs.org/docs/deployment).

The questionnaire data is loaded in memory but could easily be moved to a SQL or NoSQL database. I'd pick a PostgreSQL (running in a docker image) with Prisma. That would be reliable way that also offers better type safety. The content for the questionnaire was generated using ChatGPT. I have added some extra questionnaire titles that could easily be added in the next iteration.
