export interface Answer {
  id: number;
  content: string;
  scores: Record<string, number>;
}

export interface Question {
  id: number;
  content: string;
  answers: Answer[];
}

export type Result = {
  id: number;
  title: string;
  description: string;
};

export type Questionnaire = {
  id: number;
  title: string;
  description: string;
  questions: Question[];
  results: Record<string, Result>;
};
